FROM alpine:3.21.3

# renovate: datasource=github-tags depName=Sonarr/Sonarr versioning=loose
ENV SONARR_VER=4.0.13.2932

ARG SONARR_BRANCH=main

WORKDIR /sonarr

COPY *.sh /usr/local/bin/

RUN apk --no-cache add tini icu-libs sqlite-libs libmediainfo xmlstarlet mono --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing/

RUN wget -O- "https://github.com/Sonarr/Sonarr/releases/download/v${SONARR_VER}/Sonarr.${SONARR_BRANCH}.${SONARR_VER}.linux-musl-x64.tar.gz" \
        | tar xz --strip-components=1 \
 && find -type f -exec chmod 644 {} + \
 && find -type d -o -name '*.exe' -exec chmod 755 {} + \
 && find -name '*.mdb' -delete \
 && rm -r Sonarr.Update \
 && chmod +x /usr/local/bin/*.sh /sonarr/Sonarr /sonarr/createdump /sonarr/ffprobe \
 && /usr/bin/cert-sync /etc/ssl/certs/ca-certificates.crt

ENV XDG_CONFIG_HOME=/config

ENTRYPOINT ["/sbin/tini", "--", "/usr/local/bin/entrypoint.sh"]
CMD ["/sonarr/Sonarr", "--no-browser", "--data=/config"]
